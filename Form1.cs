﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        bool search; // Поиск/Пауза
        DateTime time; // Таймер
        int Count = 0; // Количество файлов
        string dir = ""; // Текущая директория

        private Task<List<string>> GetAllFiles(string path, string pattern)
        {
            return Task.Run(() =>
            {

                return GetFiles(path, pattern); // Получаем список файлов в данной директории по заданной маске имени файла

            });
        }

        private List<string> GetFiles(string path, string pattern)
        {
            var files = new List<string>();
            
            try
            {
                if (!search)
                {
                    return new List<string>();
                }
                files.AddRange(Directory.GetFiles(path, pattern, SearchOption.TopDirectoryOnly));
                foreach (var directory in Directory.GetDirectories(path))
                    files.AddRange(GetFiles(directory, pattern));
            }
            catch (UnauthorizedAccessException) { }

            return files;
        }

        private Task<string> CheckFile(string path, string s)
        {
            return Task.Run(() =>
            {
                return SearchInFile(path, s);
            });
        }

        private string SearchInFile(string path, string s) // Поиск по файлу
        {
            try
            {
                StreamReader sr = new StreamReader(path);
                string line;
                if (s.Equals("")) return "Найдено.";
                while ((line = sr.ReadLine()) != null)
                {
                    if (!search) return "";
                    if (line.IndexOf(s) != -1) return "Найдено.";
                }
                return "Не найдено.";
            }
            catch (Exception)
            {
                return "Не найдено.";
            }
        }

        public Form1()
        {
            InitializeComponent();
        }

        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }

        private void timer1_Tick(object sender, EventArgs e) // Обработка сигналов от таймера
        {
            time = time.AddSeconds(1);
            label1.Text = time.TimeOfDay.ToString();
        }

        private async void button1_Click(object sender, EventArgs e)
        {
            search = true;
            button1.Enabled = false;
            button2.Enabled = true; // Активация кнопки "Стоп"

            time = new DateTime(0); // Обнуление переменных
            treeView1.Nodes.Clear();
            Tree.Text = "";

            if (direct.Text.Equals(""))
            {
                MessageBox.Show("Укажите директорию поиска!");
                return;
            }

            label1.Text = time.TimeOfDay.ToString();

            timer1.Interval = 1000; // Запуск таймера
            timer1.Start();

            treeView1.BeginUpdate();
            treeView1.Nodes.Add(dir); //Корневой каталог
            treeView1.EndUpdate();

            Tree.Text += "Поиск файлов по имени..." + Environment.NewLine;
            var files = await GetAllFiles(direct.Text, "*" + FileName.Text + "*"); // Запуск ассинхронного поиска файлов
            if (!search) // Сброс функции, если нажата Пауза
            {
                button1.Enabled = true;
                button2.Enabled = false;
                timer1.Stop();
                return;
            }
            Tree.Text += "Найдено " + files.Count + " файлов." + Environment.NewLine;
            Count = files.Count;
            label5.Text = Count.ToString();
            for (int i = 0; i < files.Count; i++)
            {
                Tree.Text += "Поиск в " + files[i] + "   ";
                string nextfile = await CheckFile(files[i], Search.Text); // Запуск ассинхронного поиска в файлах
                if (!search)
                {
                    button1.Enabled = true;
                    button2.Enabled = false;
                    timer1.Stop();
                    return;
                }
                Tree.Text += nextfile + Environment.NewLine; 
                if(nextfile.Equals("Найдено."))
                {
                    AddToTree(files[i]); // Добавление в дерево папок
                }
                label5.Text = "Осталось файлов: " + --Count;
            }
            timer1.Stop();
            button1.Enabled = true;
            button2.Enabled = false;
        }

        private void AddToTree(string v) // Добавление в дерево папок
        {
            v = v.Remove(0, dir.Length + 1);
            string[] path = v.Split('\\');
            int[] dirs = new int[path.Length];
            TreeNode q = treeView1.Nodes[0];
            for (int i = 0; i < dirs.Length; i++)
            {
                dirs[i] = 0;
                bool found = false;
                for (int j = 0; j < q.Nodes.Count; j++)
                {
                    if (q.Nodes[j].Text == path[i])
                    {
                        q = q.Nodes[j];
                        dirs[i] = j;
                        found = true;
                        break;
                    }
                }
                if (!found)
                {
                    q.Nodes.Add(path[i]);
                    q = q.Nodes[q.Nodes.Count - 1];
                }
            }
            treeView1.EndUpdate();
        }

        private void direct_Click(object sender, EventArgs e) // Выбор директории
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            DialogResult result = fbd.ShowDialog();
            dir = fbd.SelectedPath;
            direct.Text = dir;
        }

        private void Tree_TextChanged(object sender, EventArgs e) // Прокрутка TextBox к концу
        {
            Tree.SelectionStart = Tree.TextLength;
            Tree.ScrollToCaret();
        }

        private void button2_Click(object sender, EventArgs e) // Стоп
        {
            search = false;
        }
    }
}
